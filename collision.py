from __future__ import division
from collections import defaultdict

from bitarray import bitarray, bitdiff
from xunyi import new_hash_function

from config import *

class __bitarray__():

    def fromstring(self, string):
        return self.frombytes(string.encode())

    def tostring(self):
        return self.tobytes().decode()

    def decode(self, codedict):
        self._check_codedict(codedict)
        return self._decode(self._mk_tree(codedict))

    def iterdecode(self, codedict):
        self._check_codedict(codedict)
        return self._iterdecode(self._mk_tree(codedict))

    def encode(self, codedict, iterable):
        self._check_codedict(codedict)
        self._encode(codedict, iterable)


def ascii_to_hex(s):

    rv = []
    for char in s:
        rv.append(hex(ord(char))[2:])

    return "0x" + "".join(rv).upper()


def binary_strings(n):
    for i in xrange(1, 2**n-1):
        yield '{:0{n}b}'.format(i, n=n)
    # ["".join(seq) for seq in itertools.product("01", repeat=n)]


def collision_test():
    collisions = 0
    colldict = defaultdict(list)
    size = 12
    # diff_list = []

    # B = bitarray(32)
    # H = bitarray(hfx(B.to01()))

    # for idx in range(0, 32):
    #     B[idx] = not B[idx]
    #     h = bitarray(hfx(B.to01()))
    #     changed.append(bitdiff(H, h))
    #     B[idx] = not B[idx]

    for msg in binary_strings(size):
        h = new_hash_function(msg)
        colldict[h].append(msg)

    for h, m in colldict.iteritems():
        # abs_diff = abs(h - hash(m))
        # diff_list.append(abs_diff)
        if len(colldict[h]) > 1:
            collisions += len(colldict[h]) - 1

    # print("Number of collisions = %d" % collisions)
    # print("Number of inputs that map to same hash = %d" % len(max(d.values(), key=len)))

if __name__ == '__main__':
    collision_test()

    from textwrap import dedent

    print("Absolute difference of hashes: ")
    print(dedent("""
        Maximum: %d
        Minimum: %d
        Mean: %d""" % (collisions['max'](), collisions['min'](), collisions['mean']())))
