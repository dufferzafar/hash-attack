from __future__ import division
from collections import defaultdict

from xunyi import new_hash_function


def binary_strings(n):
    # ["".join(seq) for seq in itertools.product("01", repeat=n)]
    for i in xrange(1, 2**n-1):
        yield '{:0{n}b}'.format(i, n=n)

N = 12
collisions = 0
d = defaultdict(list)

if __name__ == '__main__':

    # TODO: For size 64?
    for msg in binary_strings(N):
        h = new_hash_function(msg)
        d[h].append(msg)

    for h, m in d.iteritems():
        if len(d[h]) > 1:
            collisions += len(d[h]) - 1

    print("Number of collisions = %d" % collisions)
    print("Fraction of collisions = %s" % (collisions / (2 ** N)))

    print("Number of inputs that map to same hash = %d" % len(max(d.values(), key=len)))
