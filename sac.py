"""Test out the hash functions."""
from __future__ import division

from bitarray import bitarray, bitdiff
import random
from textwrap import dedent

from config import *


def statistics(hfx, N=32):
    B = bitarray(N)
    H = bitarray(hfx(B.to01()))

    changed = []

    for idx in range(0, 32):
        B[idx] = not B[idx]
        h = bitarray(hfx(B.to01()))
        changed.append(bitdiff(H, h))
        B[idx] = not B[idx]

    bmin = min(changed)
    bmax = max(changed)
    bmean = sum(changed) / N
    pmean = (bmean / 22) * 100

    print("Results of strict avalanche criterion: ")
    print(dedent("""
    Minimum number of changed bits: %d
    Maximum number of changed bits: %d
    Mean number of changed bits: %f
    Mean changed probability: %f
    """) % (sac['bmin'](), sac['bmax'](), sac['bmean'](), sac['pmean']()))


if __name__ == '__main__':

    from xunyi import new_hash_function

    statistics(new_hash_function)
