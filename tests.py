"""Test out the hash functions."""
from __future__ import division

from bitarray import bitarray, bitdiff


def statistics(hfx, N=32):
    B = bitarray(N)
    # B = bitarray(''.join([str(random.choice([0, 1])) for _ in range(0, 128)]))
    H = bitarray(hfx(B.to01()))

    changed = []

    for idx in range(0, 32):

        # idx = random.randint(0, N)

        B[idx] = not B[idx]

        h = bitarray(hfx(B.to01()))
        changed.append(bitdiff(H, h))

        B[idx] = not B[idx]

    min_ = min(changed)
    max_ = max(changed)
    mean_b = sum(changed) / N
    mean_p = (mean_b / 22) * 100

    print("""
    Minimum changed bit number: %d
    Maximum changed bit number: %d
    Mean changed bit number: %f
    Mean changed probability: %f
    """ % (min_, max_, mean_b, mean_p))


if __name__ == '__main__':

    from xunyi import new_hash_function

    statistics(new_hash_function)
