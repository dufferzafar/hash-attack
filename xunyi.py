u"""
Implements the hash function from paper:

"Hash Function Based on Chaotic Tent Maps" by Xun Yi
(IEEE Transactions on Circuits and Systems-II: Express Briefs,
Vol. 52, No. 6, June 2005)
"""

from bitarray import bitarray


class __bitarray__():

    def fromstring(self, string):
        return self.frombytes(string.encode())

    def tostring(self):
        return self.tobytes().decode()

    def decode(self, codedict):
        self._check_codedict(codedict)
        return self._decode(self._mk_tree(codedict))

    def iterdecode(self, codedict):
        self._check_codedict(codedict)
        return self._iterdecode(self._mk_tree(codedict))

    def encode(self, codedict, iterable):
        self._check_codedict(codedict)
        self._encode(codedict, iterable)


def min_max_mean(l):
    """Returns minimum, maximum, and mean of a list of integers."""
    return (min(l), max(l), (sum(l) / len(l)))


def str_to_bits(s):
    rv = bitarray()
    rv.frombytes(s)
    return rv.to01()


def float_to_bits(x):
    x = str(repr(x))
    return bin(int(x[x.find('.')+1:]))[2:]


def bits_to_hex(b):
    rv = []
    for pos in range(0, len(b), 8):
        byte = b[pos:pos+8]
        rv.append(hex(int(byte, base=2))[2:])

    return "0x" + "".join(rv).upper()


def ascii_to_hex(s):

    rv = []
    for char in s:
        rv.append(hex(ord(char))[2:])

    return "0x" + "".join(rv).upper()

def blocks(msg, size, append_length=True):
    """
    Return 'size' sized 'msg' blocks represented as 'bitarray's.

    Optionally pad the message and append message length.
    """
    if append_length:
        msg += bin(len(msg))[2:]
    msg.zfill((len(msg) // size) + 1)
    for i in range(0, len(msg), size):
        # yield bitarray(msg[i:i+size])
        yield msg[i:i+size]


# Equation no. (1) as defined in the paper
def Fmap(x, alpha):
    """Piecewise Linear Map"""
    # TODO: Iterations in Fmap?

    if 0 <= x <= alpha:
        return (x - 1) / alpha
    elif alpha < x <= 1:
        return (1 - x) / (1 - alpha)
    else:
        raise ValueError("Bad x or alpha value passed.")


# Equation no. (2) as defined in the paper
# The paper chose 75 as the number of iterations.
def Gmap(x, alpha, iters=75):

    # beta is a constant between 0, 1
    beta = 0.141572

    for _ in range(0, iters):

        if 0 < x < 1:
            # alpha gets mapped from [0, 1) to a sufficiently
            # small interval around 1/2
            x = Fmap(x, alpha=0.4875+0.1*alpha)
        else:
            x = beta

    return x


def hash_round_function(si, ti, mi, mir):

    # Function no. (3) & (4) as defined in the paper.
    def box(x, y):
        return (x + y) % 1

    def circle(x, y):
        return Gmap(x=max(x, y), alpha=min(x, y))

    g = Gmap(x=box(si, mi), alpha=box(ti, mir))
    c = circle(ti, box(mi, g))

    # Final results to be sent!
    s = c
    t = box(g, si)

    return s, t


# The algorithm as defined in section II. B
def new_hash_function(message, block_size=16):
    # s0, t0
    si = 0.10110101
    ti = 0.10110101

    for Mi in blocks(message, size=block_size):
        mi = float("0." + Mi)
        mir = float("0." + Mi[::-1])
        si, ti = hash_round_function(si, ti, mi, mir)

    hash_result = float_to_bits(si) + float_to_bits(ti)
    return hash_result[:2*block_size]


if __name__ == '__main__':

    input_text = "random"

    m = str_to_bits(input_text)
    h = new_hash_function(m)

    # print("Input text (binary): %s" % m)
    print("Input text (hex) %s " % ascii_to_hex(input_text))
    # print("Input text (hex) %s " % bits_to_hex(m))

    print("Output hash (hex): %s" % bits_to_hex(h))
