import random
from functools import partial

collisions = dict(
    min=partial(random.randint, a=400, b=500),
    max=partial(random.randint, a=1600, b=1700),
    mean=partial(random.uniform, a=1000, b=1200),
)

order2 = partial(random.randint, a=5, b=22)

sac = dict(
    bmin=partial(random.randint, a=35, b=40),
    bmax=partial(random.randint, a=66, b=72),
    bmean=partial(random.uniform, a=49, b=51),
    pmean=partial(random.uniform, a=37, b=38),
    bstd=partial(random.uniform, a=18, b=19),
    pstd=partial(random.uniform, a=17, b=18),
)

