default: clean

clean:
	@find . -name "__pycache__" -delete
	@find . -name "*.pyc" -delete
