from textwrap import dedent
from xunyi import bits_to_hex

from bitarray import bitarray, rand_bitarray

from config import *


class __bitarray__():

    def fromstring(self, string):
        return self.frombytes(string.encode())

    def tostring(self):
        return self.tobytes().decode()

    def decode(self, codedict):
        self._check_codedict(codedict)
        return self._decode(self._mk_tree(codedict))

    def iterdecode(self, codedict):
        self._check_codedict(codedict)
        return self._iterdecode(self._mk_tree(codedict))

    def encode(self, codedict, iterable):
        self._check_codedict(codedict)
        self._encode(codedict, iterable)


def second_order(hfx, N=32):
    """
    Second Order Differential Attack

    Implemented from the algorithm presented in:
    https://securewww.esat.kuleuven.be/cosic/publications/thesis-223.pdf (page 60)
    """

    test_cases = 1000
    # num_bits = random.choice(range(0,N)) # random number of bits
    num_bits = 128
    max_attack = 0
    max_len = 0
    block_size = 16
    rv = []

    for idx in range(0, test_cases):

        # Random variables
        B = rand_bitarray(num_bits)
        a1 = rand_bitarray(num_bits)
        a2 = rand_bitarray(num_bits)

        f = bitarray(hfx(B.to01()))  # f(x)

        x1 = B ^ a2  # x + a2
        x2 = B ^ a1 ^ a2  # x + a1 + a2
        x3 = B ^ a1  # x + a1

        f1 = bitarray(hfx(x1.to01()))  # f(x + a2)
        f2 = bitarray(hfx(x2.to01()))  # f(x + a1 + a2)
        f3 = bitarray(hfx(x3.to01()))  # f(x + a1)

        # print (f, f1, f2, f3)
        result = f ^ f1 ^ f2 ^ f3  # f - f1 + f2 - f3
        # print(result.to01())

        count_zero = 0
        result = result.to01()

        for idx in result:
            if idx == '0':
                count_zero = count_zero + 1
            else:
                break

        rv.append(order2())

        if count_zero == len(result):
            print(dedent("""
            B (x): %s
            a1: %s
            a2: %s
            Number of bits: %d
            """) % (bits_to_hex(B.to01()), bits_to_hex(a1.to01()), bits_to_hex(a2.to01()), num_bits))
        # else:
        #     print("Couldn't found collision for full bits")
        #     print("Instead attack will be successfull on %d bits out of %d bits." % (count_zero, result))
        #     print("For a inputs bits of %d bits" % num_bits)

        if max_attack < count_zero:
            max_attack = count_zero
            max_len = len(result)

    return rv


if __name__ == '__main__':

    from xunyi import new_hash_function, min_max_mean
    results = second_order(new_hash_function)

    print("Absolute difference of hashes: ")
    print(dedent("""
        Maximum: %d
        Minimum: %d
        Mean: %d""" % min_max_mean(results)))
